import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/admin/main.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
 y:any[];
  user;
  
  constructor(private ds:MainService,private router:Router) { }

  ngOnInit() {
    
    this.user=this.ds.sendloggeduser();
    this.ds.viewspecificprofile(this.user).subscribe(profile=>{
      // this.y=profile['message'];
      if(profile['message']=="unauthorized access")
      {
        alert(profile['message']);
        this.router.navigate(['/header/login']);
      }
      else{
        this.y=profile['message']
      }
  })

  }

}
