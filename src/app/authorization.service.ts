import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs';
@Injectable({
  providedIn:'root'
})

export class authorizationservice implements HttpInterceptor{
  intercept(req:HttpRequest<any>,next:HttpHandler):Observable<HttpEvent<any>>
  {
    //read token from local storage
    const idToken=localStorage.getItem("idToken");
    //if token is found it adds it to header of request object
  
    if(idToken)
    {
    const cloned=req.clone({
      headers:req.headers.set("Authorization","Bearer "+idToken)
    });
    //and then forward the request object cloned with token
    console.log(cloned);
    return next.handle(cloned);
    
  }
  else{
    return next.handle(req);
  }
}
constructor() {}
}
