import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/admin/main.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  
  data:any;
  constructor(private ds:MainService) { }

  ngOnInit() {
    this.ds.sendtostudent().subscribe(data=>{
      this.data=data['message'];
    });
  }

}
