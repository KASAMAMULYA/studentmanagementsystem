import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileComponent } from './profile/profile.component';
import { MarksComponent } from './marks/marks.component';
import { FeeComponent } from './fee/fee.component';

import { Student1Component } from './student1/student1.component';
import { AttendenceComponent } from './attendence/attendence.component';
import { NotificationComponent } from './notification/notification.component';
import { LeaveComponent } from './leave/leave.component';

const routes: Routes = [{
  path:"student1",
  component:Student1Component,
  children:[{
    path:'profile',
    component:ProfileComponent
  },
  {
    path:'marks',
    component:MarksComponent
  },
  {
    path:'fee',
    component:FeeComponent
  },
  {
    path:'attendence',
    component:AttendenceComponent
  },
  {
    path:'notification',
    component:NotificationComponent
  },
  {
    path:'leave',
    component:LeaveComponent
    }
    
]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
