import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './header/home/home.component';
import { AboutUsComponent } from './header/about-us/about-us.component';
import { LoginComponent } from './header/login/login.component';

import { AdminModule } from './admin/admin.module';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';


import { StudentModule } from './student/student.module';
import { authorizationservice } from './authorization.service';
import { ForgotComponent } from './header/forgot/forgot.component';
import { OtpComponent } from './header/otp/otp.component';
import { ChangepwdComponent } from './header/changepwd/changepwd.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AboutUsComponent,
    LoginComponent,
    ForgotComponent,
    OtpComponent,
    ChangepwdComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,FormsModule,
    AdminModule,
    HttpClientModule,
    StudentModule
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass:authorizationservice,
      multi:true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }