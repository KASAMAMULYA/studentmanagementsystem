//create exp app
const exp=require("express");
const app=exp();
const bodyparser=require("body-parser");
app.use(bodyparser.json());
//import path module
var path=require("path")
app.use(exp.static(path.join(__dirname,'../dist/project1')));
//import adminroutes
const adminroutes=require('./routes/adminroutes');
//import user 
const studentroutes=require('./routes/studentroutes');
//informing to server about routes
app.use("/admin",adminroutes);
app.use("/student",studentroutes);
app.use((err,req,res,next)=>{
    console.log(err);
})
//assigning port number
const port=8080;
app.listen(process.env.PORT || port,()=>console.log(`server running on port ${port}`));
