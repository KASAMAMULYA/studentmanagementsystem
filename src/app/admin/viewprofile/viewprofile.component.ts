import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.css']
})
export class ViewprofileComponent implements OnInit {
  data:any[];
  l:any[]=[];
  searchterm:any;
  constructor(private ds:MainService,private as:HttpClient,private route:Router) { }

  ngOnInit() {
    this.ds.dataToadmin().subscribe(l=>{
      console.log(l['message']);
      if(l['message']=="unauthorized access")
      {
        alert(l['message']);
        this.route.navigate(['/header/login']);
      }
      else{
        this.l=l['message']
      }

      // this.l=l['message'];
    })
  }

  b:boolean=false;
  objectToModify:object;
  editingData(obj)
  {
    this.objectToModify=obj;
    console.log(this.objectToModify);
    this.b=true;
  }
  testing(value)
  {
    
    
    this.b=false;
    this.as.put('admin/updateprofile',value).subscribe(res=>{
      if(res['message']=="success")
      {
        alert("successfully modified");
      }
    })
  }

  deleteRecord(studentname)
  {
    console.log(studentname);
    this.as.delete(`admin/deleteprofile/${studentname}`).subscribe(res=>{
      alert(res['message']);
      this.data=res['data'];

    })
  }

}
