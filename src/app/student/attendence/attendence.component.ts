import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/admin/main.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-attendence',
  templateUrl: './attendence.component.html',
  styleUrls: ['./attendence.component.css']
})
export class AttendenceComponent implements OnInit {
  user;
  y:any[];
  constructor(private ds:MainService ,private router:Router) { }


  ngOnInit() {
    this.user=this.ds.sendloggeduser();
    this.ds.viewspecificattendence(this.user).subscribe(fee=>{
      if(fee['message']=="unauthorized access")
      {
        alert(fee['message']);
        this.router.navigate(['/header/login']);
      }
      else{
        this.y=fee['message']
      }
  })

  
}
}

