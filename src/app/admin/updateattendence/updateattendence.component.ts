import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-updateattendence',
  templateUrl: './updateattendence.component.html',
  styleUrls: ['./updateattendence.component.css']
})
export class UpdateattendenceComponent implements OnInit {
  data:any[];
  searchterm:any;

  constructor(private ds:MainService,private at:HttpClient,private router:Router) { }

  ngOnInit() {
    this.ds.attendencestatus().subscribe(data=>{
      // this.d=data['message'];
      // console.log(this.d);
      if(data['message']=="unauthorized access")
      {
        alert(data['message']);
        this.router.navigate(['/header/login']);
      }
      else{
        this.data=data['message']
      }
  })
}


  attendenceData(value)
  {
  //this.d=value;
  //this.ds.receivedData1(this.d);
  this.at.post('admin/updateattendence',value).subscribe(res=>{
    alert(res['message']);    })
}


b:boolean=false;
  objectToModify:object;
  editingattendence(obj)
  {
    this.objectToModify=obj;
    console.log(this.objectToModify);
    this.b=true;
  }
  attendence(value)
  {
    this.b=false;
    this.at.put('admin/updateattendence',value).subscribe(res=>{
      if(res['message']=="success")
      {
        alert("successfully modified");
      }
    })
  }
  deleteRecord(studentname)
  {
    console.log(studentname);
    this.at.delete(`admin/deleteattendence/${studentname}`).subscribe(res=>{
      alert(res['message']);
      this.data=res['data'];

    })
  }
}