import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostingmarksComponent } from './postingmarks.component';

describe('PostingmarksComponent', () => {
  let component: PostingmarksComponent;
  let fixture: ComponentFixture<PostingmarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostingmarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostingmarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
