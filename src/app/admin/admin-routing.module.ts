import { NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostingmarksComponent } from './postingmarks/postingmarks.component';
import { UpdatefeestatusComponent } from './updatefeestatus/updatefeestatus.component';
import { UpdateattendenceComponent } from './updateattendence/updateattendence.component';
import { SendnotificationComponent } from './sendnotification/sendnotification.component';
import { AdminComponent } from './admin/admin.component';

import { SendleaveComponent } from './sendleave/sendleave.component';
import { AddstudentComponent } from './addstudent/addstudent.component';
import { ViewprofileComponent } from './viewprofile/viewprofile.component';

const routes: Routes = [
    {
      path:'admin',
      component:AdminComponent,
      children:[
        {
          path:'postingmarks',
          component:PostingmarksComponent
        },
        {
          path:'updatefeestatus',
          component:UpdatefeestatusComponent
        },
        {
          path:'updateattendence',
          component:UpdateattendenceComponent
        },

        {
          path:'sendnotification',
          component:SendnotificationComponent
        },
        {
          path:'sendleave',
          component:SendleaveComponent
          },
          {
            path:'addstudent',
            component:AddstudentComponent
          },
          {
            path:'viewprofile',
            component:ViewprofileComponent
          },
          
          
          
        ]
    }
    

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
