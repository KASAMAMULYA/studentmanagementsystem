import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {

  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit() {

  }
 sendotp(x)
  {
    this.http.post('/admin/forgotpassword',x).subscribe((res)=>
    {
      alert(res['message'])
      if(res['message']=="user found")
      {
        this.router.navigate(['/header/otp'])
      }
      else
      {
        this.router.navigate(['/header/otp'])
      }
    })
}
}
