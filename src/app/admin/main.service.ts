import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable} from 'rxjs';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class MainService {
  constructor(private hc:HttpClient,private router:Router){}
  b:any[]=[];
 
  receivedData(a)
  {
    this.b=a;
  }
  sendData()
  {
    return this.b;
  }
  h:any[];
  receivedData1(d)
  {
    this.h=d;
  }
  sendData1()
  {
    return this.h;
  }
  o:any[];
  receivedData2(k)
  {
   this.o=k
  }
  sendData2()
  {
    return this.o;
  }
  n:number;
  receivepending(pending)
  {
    this.n=pending;
  }
  sendpending()
  {
    return this.n;
  }
  i:any[]=[];
  receivedfromstudent(v)
  {
    this.i=v;
  }
  dataToadmin():Observable<any[]>  {

    
    return this.hc.get<any[]>('/student/profile');
    
  }
  t:any;
  receivedDatatoservice(r)
  {
    this.t=r;
  }

  sendtostudent():Observable<any[]>
  {
    return this.hc.get<any[]>('student/Notification');
  }
  e:any;
  sendLeaveToAdmin(a)
  {
this.e=a;
  }
  sendLeave():Observable<any[]>
  {
    return this.hc.get<any[]>('admin/sendLeave');
  }
  f:any;
  reciveaccept(e)
  {
    this.f=e;
  }
  sendAccept()
  {
    return this.f;
  }
  j:any;
  recivereject(h)
  {
    this.j=h;
  }
  sendReject()
  {
    return this.j;
  }
  marksstatus():Observable<any[]>{
    return this.hc.get<any[]>('student/marks');
  }
  feestatus():Observable<any[]>{
    return this.hc.get<any[]>('student/fee');
  }
  attendencestatus():Observable<any[]>{
    return this.hc.get<any[]>('student/attendence');
  }
doLogin(userobject):Observable<any>
{
  console.log(userobject)
  return this.hc.post<any[]>('student/login',userobject);
}
logout()
{
  localStorage.removeItem("idToken");
    this.router.navigate(['/header']);
}
adminLogin(userobject):Observable<any>
{
  return this.hc.post<any>('admin/login',userobject);
}
// leaverequest(user)
// {
//   console.log(user);
//   return this.hc.post('student/specificstnrequest',user);
// }
user;
loggeduser(user)
{
  console.log(user);
  this.user=user[0];
}
sendloggeduser()
{
  return this.user;
}
viewspecificmarks(user)
{
  console.log(user);
  return this.hc.post('student/viewspecificmarks',user);
}
viewspecificfee(user)
{
  console.log(user);
  return this.hc.post('student/viewspecificfee',user);
}
viewspecificattendence(user)
{
  console.log(user);
  return this.hc.post('student/viewspecificattendence',user);
}
viewspecificprofile(user)
{
  console.log(user);
  return this.hc.post('student/viewspecificprofile',user);
}
viewspecifications3(user)
{ 
  return this.hc.post<any[]>('student/viewspecificreq',user)
}
}
