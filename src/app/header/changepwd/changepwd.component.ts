import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-changepwd',
  templateUrl: './changepwd.component.html',
  styleUrls: ['./changepwd.component.css']
})
export class ChangepwdComponent implements OnInit {

  constructor(private hc:HttpClient,private router:Router) { }

  ngOnInit() {
  }
  changepwd(z)
  {
    this.hc.put('/admin/changepassword',z).subscribe((res)=>{
      if(res['message']=='password changed')
      {
        alert(res['message'])
        this.router.navigate(['/header/login'])
      }
      alert(res['message'])
     
    })
  }

}
