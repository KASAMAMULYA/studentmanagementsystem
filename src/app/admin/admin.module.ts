import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms'
import { AdminRoutingModule } from './admin-routing.module';
import { PostingmarksComponent } from './postingmarks/postingmarks.component';
import { UpdatefeestatusComponent } from './updatefeestatus/updatefeestatus.component';
import { UpdateattendenceComponent } from './updateattendence/updateattendence.component';
import { SendnotificationComponent } from './sendnotification/sendnotification.component';
import { AdminComponent } from './admin/admin.component';


import { SendleaveComponent } from './sendleave/sendleave.component';
import { AddstudentComponent } from './addstudent/addstudent.component';
import { ViewprofileComponent } from './viewprofile/viewprofile.component';

import { SearchPipe } from './search.pipe';

@NgModule({
  declarations: [PostingmarksComponent, UpdatefeestatusComponent, UpdateattendenceComponent, SendnotificationComponent, AdminComponent, SendleaveComponent, AddstudentComponent, ViewprofileComponent, SearchPipe],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule
  ]
})
export class AdminModule { }
