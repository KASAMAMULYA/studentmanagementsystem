import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/admin/main.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.css']
})
export class MarksComponent implements OnInit{
    y:any[]=[];
    user;
  constructor(private ds:MainService,private router:Router) { }
  ngOnInit() {
    this.user=this.ds.sendloggeduser();
    this.ds.viewspecificmarks(this.user).subscribe(marks=>{
      if(marks['message']=="unauthorized access")
      {
        alert(marks['message']);
        this.router.navigate(['/header/login']);
      }
      else{
        this.y=marks['message']
      }
  })

}
}
