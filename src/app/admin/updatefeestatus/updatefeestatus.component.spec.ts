import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatefeestatusComponent } from './updatefeestatus.component';

describe('UpdatefeestatusComponent', () => {
  let component: UpdatefeestatusComponent;
  let fixture: ComponentFixture<UpdatefeestatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatefeestatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatefeestatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
