import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import {HttpClient} from '@angular/common/http'
import { Router } from '@angular/router';
@Component({
  selector: 'app-updatefeestatus',
  templateUrl: './updatefeestatus.component.html',
  styleUrls: ['./updatefeestatus.component.css']
})
export class UpdatefeestatusComponent implements OnInit {
  data:any[];  
  pending:number;
  p2:number=0;
b:boolean=false;
searchterm:any;
  constructor(private ds:MainService,private fe:HttpClient,private router:Router) { }
  ngOnInit() {
    this.ds.feestatus().subscribe(data=>{
      // this.k=data['message'];
      if(data['message']=="unauthorized access")
      {
        alert(data['message']);
        this.router.navigate(['/header/login']);
      }
      else{
        this.data=data['message']
      }
  })
}

  fee(value)
  {
    // this.k=value;
    this.pending=this.p2+value.total-value.paid;
    // this.ds.receivepending(this.pending);
    this.fe.post('admin/updatefeestatus',value).subscribe(res=>{
      alert(res["message"]);
     })
  }
  objectToModify:object;
  editingfee(obj)
  {
    this.objectToModify=obj;
    console.log(this.objectToModify);
    this.b=true;
  }
  testingfee(value)
  {
    this.b=false;
    this.fe.put('admin/updatefeestatus',value).subscribe(res=>{
      if(res['message']=="success")
      {
        alert("successfully modified");
      }
    })
  }

 deleteRecord(studentname)
  {
    console.log(studentname);
    this.fe.delete(`admin/deletefee/${studentname}`).subscribe(res=>{
      alert(res['message']);
      this.data=res['data'];

    })
  }
}
