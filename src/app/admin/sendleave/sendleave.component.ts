import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sendleave',
  templateUrl: './sendleave.component.html',
  styleUrls: ['./sendleave.component.css']
})
export class SendleaveComponent implements OnInit {
  data:any[]=[]
  c:boolean=false
  loggeduser;
    constructor(private service:MainService,private http:HttpClient,private router:Router) { }
    ngOnInit() {
      this.http.get('/admin/readreq').subscribe(data=>{
        if(data['message']=="unauthorized access")
        {
          alert(data['message'])
         this.router.navigate(["/main/login"])
        }
        else{
        this.data=data['message']
        }
      })
    }
  accept(username)
  {
    this.loggeduser=this.service.sendloggeduser();
    this.http.post('/admin/savereq',({"message":"request is accepted","username":username})).subscribe((res)=>{
      alert(res['message'])
    })
  }
  reject()
  {
    this.c=true;
  }
  response(x,username)
  {
    
    this.c=false
    x.username=username
    x.message="Request is rejected"
    this.loggeduser=this.service.sendloggeduser();
    this.http.post('admin/savereq',x).subscribe((res)=>{
      alert(res['message'])
      this.http.get('/admin/readreq').subscribe(data=>{
        this.data=data['message']
      })
      
    })
  }
}