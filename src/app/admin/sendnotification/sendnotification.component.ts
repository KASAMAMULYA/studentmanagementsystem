import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import {HttpClient} from '@angular/common/http'

@Component({
  selector: 'app-sendnotification',
  templateUrl: './sendnotification.component.html',
  styleUrls: ['./sendnotification.component.css']
})
export class SendnotificationComponent implements OnInit {
    r:any;
  constructor(private ds:MainService,private se:HttpClient) { }
  notifications(x)
  {
  this.r=x;
  this.ds.receivedDatatoservice(this.r);
  this.se.post('admin/sendnotification',x).subscribe(res=>{
    alert(res['message']);    })

  }
  ngOnInit() {
  }

}
