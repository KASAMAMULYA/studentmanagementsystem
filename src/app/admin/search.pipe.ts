import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(data:any,searchterm:any): any {
    if(!searchterm)
    {
      return data;
    }
    return data.filter(data=>data.username.toLowerCase().indexOf(searchterm.toLowerCase())!==-1);
  }

}
