import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms'
import { StudentRoutingModule } from './student-routing.module';

import { ProfileComponent } from './profile/profile.component';
import { MarksComponent } from './marks/marks.component';
import { FeeComponent } from './fee/fee.component';
import { NotificationComponent } from './notification/notification.component';
import { Student1Component } from './student1/student1.component';
import { AttendenceComponent } from './attendence/attendence.component';
import { LeaveComponent } from './leave/leave.component';

@NgModule({
  declarations: [ ProfileComponent, MarksComponent, FeeComponent, NotificationComponent, Student1Component, AttendenceComponent, LeaveComponent],
  imports: [
    CommonModule,
    StudentRoutingModule,FormsModule
  ]
})
export class StudentModule { }
