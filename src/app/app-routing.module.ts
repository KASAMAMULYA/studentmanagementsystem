import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './header/home/home.component';
import { AboutUsComponent } from './header/about-us/about-us.component';
import { LoginComponent } from './header/login/login.component';
import { HeaderComponent } from './header/header.component';
import { ForgotComponent } from './header/forgot/forgot.component';
import { OtpComponent } from './header/otp/otp.component';
import { ChangepwdComponent } from './header/changepwd/changepwd.component';

const routes: Routes = [
  
  {
    path:'',
    redirectTo:'header',
    pathMatch:'full'
  },
  
  
  {
    path:'header',
    component:HeaderComponent,
     children:[{
      
        path:'',
        redirectTo:'home',
        pathMatch:'full'
      },
      {
        path:'home',
        component:HomeComponent
      },

      {
        path:'aboutUs',
        component:AboutUsComponent
      },
      {
        path:'login',
        component:LoginComponent
      },
      {
        path:'forgot',
        component:ForgotComponent
      },
      {
        path:'otp',
        component:OtpComponent
      },
      {
        path:'changepwd',
        component:ChangepwdComponent
      },
     ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
