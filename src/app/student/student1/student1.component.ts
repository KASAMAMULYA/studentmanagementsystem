import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student1',
  templateUrl: './student1.component.html',
  styleUrls: ['./student1.component.css']
})
export class Student1Component implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  log()
  {
    localStorage.removeItem('idToken');
    this.router.navigate(['/header']);
  }
}
