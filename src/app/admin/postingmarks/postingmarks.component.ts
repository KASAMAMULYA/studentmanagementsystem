import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-postingmarks',
  templateUrl: './postingmarks.component.html',
  styleUrls: ['./postingmarks.component.css']
})
export class PostingmarksComponent implements OnInit {
  a:any[]=[]
  data:any[]=[]
  n:any[];
  searchterm:any;
  courses:string[]=['B.Tech','M.Tech','MBA'];
  branches:string[]=['civil','cse','ece'];
  constructor(private ds:MainService,private ms:HttpClient,private router:Router) { }
  marksData(value)
  {
    this.n=value;
  this.a=value;
  this.ds.receivedData(this.a);
  this.ms.post('admin/postingmarks',value).subscribe(res=>{
    alert(res['message']);  
    })

  }
  b:boolean=false;
  objectToModify:object;
  editingmarks(obj)
  {
    this.objectToModify=obj;
    
    console.log(this.objectToModify);
    this.b=true;
  }
  testingmarks(value)
  {
    this.b=false;
    this.ms.put('admin/postingmarks',value).subscribe(res=>{
      if(res['message']=="success")
      {
        alert("successfully modified");
      }
    })
  }

  ngOnInit() {
    this.ds.marksstatus().subscribe(data=>{
      // this.a=data['message'];
      if(data['message']=="unauthorized access")
      {
        alert(data['message']);
        this.router.navigate(['/header/login']);
      }
      else{
        this.data=data['message']
      }
    })
  }
  deleteRecord(studentname)
  {
    console.log(studentname);
    this.ms.delete(`admin/deletemarks/${studentname}`).subscribe(res=>{
      alert(res['message']);
      this.a=res['data'];
     
    })
  }
}
