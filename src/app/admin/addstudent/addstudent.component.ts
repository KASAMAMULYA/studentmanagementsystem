import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import {HttpClient} from '@angular/common/http'
import { Router } from '@angular/router';

@Component({
  selector: 'app-addstudent',
  templateUrl: './addstudent.component.html',
  styleUrls: ['./addstudent.component.css']
})
export class AddstudentComponent implements OnInit {
  constructor(private ds:MainService,private as:HttpClient,private route:Router) { }
l:any[];
  ngOnInit() {
    // this.ds.dataToadmin().subscribe(l=>{
    //   console.log(l['message']);
    //   if(l['message']=="unauthorized access")
    //   {
    //     alert(l['message']);
    //     this.route.navigate(['/header/login']);
    //   }
    //   else{
    //     this.l=l['message']
    //   }

    //   // this.l=l['message'];
    // })
  }
  data:any[];
  val:any[];
  v:any[];
  skills:string[]=['cpp','java','angular'];
  courses:string[]=['b.tech','m.tech','mba'];
  branches:string[]=['civil','cse','ece']
  Data(value)
  {
    if(value.studentno=="" || value.username=="" ||value.password=="")
    {
      alert("enter valid data")
    }
    else{
    console.log(value);

     this.val=value;
    this.v=value;
   this.ds.receivedfromstudent(this.v);
    this.as.post('admin/addstudent',value).subscribe(res=>{
      alert(res['message']);    })
    
  
  }
}

  // b:boolean=false;
  // objectToModify:object;
  // editingData(obj)
  // {
  //   this.objectToModify=obj;
  //   console.log(this.objectToModify);
  //   this.b=true;
  // }
  // testing(value)
  // {
    
    
  //   this.b=false;
  //   this.as.put('admin/updateprofile',value).subscribe(res=>{
  //     if(res['message']=="success")
  //     {
  //       alert("successfully modified");
  //     }
  //   })
  // }

  // deleteRecord(studentname)
  // {
  //   console.log(studentname);
  //   this.as.delete(`admin/deleteprofile/${studentname}`).subscribe(res=>{
  //     alert(res['message']);
  //     this.data=res['data'];

  //   })
  // }
}
