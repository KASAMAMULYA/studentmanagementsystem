import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/admin/main.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-leave',
  templateUrl: './leave.component.html',
  styleUrls: ['./leave.component.css']
})
export class LeaveComponent implements OnInit {
  constructor(private service:MainService,private http:HttpClient,private router:Router) { }
  accept:any[]=[]
  b:boolean=true
  loggeduser;
  user;
  ngOnInit() {
    this.user=this.service.sendloggeduser()
    this.service.viewspecifications3(this.user).subscribe(accept=>{
      if(accept['message']=="unauthorized access")
      {
        alert(accept['message'])
        this.router.navigate(["/header/login"])
      }
      else{
      this.accept=accept['message']
      }
    })
   }
  requestof(x)
    {
      console.log(x)
     this.loggeduser=this.service.sendloggeduser();
     x.username=this.loggeduser.username
     this.http.post('/student/savereq',x).subscribe(res=>{
       alert(res["message"])
       this.user=this.service.sendloggeduser()
       this.service.viewspecifications3(this.user).subscribe(accept=>{
        this.accept=accept['message']
       })
     })
     this.b=false
    }
  }