//creates a express mini app to handle admin routes
const exp=require("express");
var adminroutes=exp.Router();
const bcrypt=require('bcrypt')
var dbo;
const getDb=require("../DBconfig").getDb;
const initDb=require("../DBconfig").initDb;

const jwt=require("jsonwebtoken");
const secretkey="secret";
const nodemailer=require('nodemailer')
const checkAuthorization=require('../middlewares/checkAuthorization');
const accountSid = 'ACc4872299c716a04d44a9d6875af9d753';
const authToken = '40dd7774712eec454f9d0f2623001ad3';
const client = require('twilio')(accountSid, authToken);

//import bodyparser
var bodyparser=require("body-parser");
adminroutes.use(bodyparser.json());
initDb();
//admin route handler
adminroutes.get('/read',(req,res)=>{
    dbo=getDb();
});
adminroutes.post('/login',(req,res,next)=>{
    console.log("hi......");
    console.log(req.body);
    dbo=getDb();
    // if(req.body.usertype==="admin")
    // {
        dbo.collection('admin2collection').find({username:{$eq:req.body.username}}).toArray((err,success)=>{
            console.log(success.length);
          if(success.length==0)
            {
                res.json({message:"Invalid username"});
            }
           else if(success[0].password!==req.body.password)
            {
                 res.json({message:"Invalid password"});   
           }
            else{
             const signedtoken=jwt.sign({username:success[0].username},secretkey,{expiresIn:"7d"});
                    console.log("signedtoken is",signedtoken);
                 res.json({message:"success",token:signedtoken});
           }
        })
})

adminroutes.post('/addstudent',checkAuthorization,(req,res,next)=>{
    const transport=nodemailer.createTransport({
      service:'Gmail',
      auth:{
          user:'amulyakasam@gmail.com',
          pass:'xxxxxxxxxxx',
      },
    });
    const mailOption={
        from:'amulyakasam@gmail.com',
        to:req.body.gmail,
        subject:'student credentials',
        text:`username:${req.body.username},password:${req.body.pass}`

    };
    transport.sendMail(mailOption,(error,info)=>{
        if(error)
        {
            console.log(error);
        }
    });
    console.log(11111111111111111121212);
    console.log(req.body);
    dbo=getDb();
    dbo.collection('profilecollection').find({username:{$eq:req.body.username}}).toArray((err,result)=>{
        if(result=="")
        {
    bcrypt.hash(req.body.password,6,(err,hashedpassword)=>{
        if(err)
        {
            next(err);
        }
        else{
            //replace plaintext password with hashed password
            req.body.password=hashedpassword;
            dbo.collection("profilecollection").insert(req.body,(err,success)=>{
                if(err)
                {
                    console.log('err in admin register');
                }
                else{
                    res.json({message:"registration success"})
                }
            })
            
        }
    })
}

    else{
        res.json({"message":"dupilcate"});
        console.log("duplicate",result)
    }
})
});


adminroutes.post('/postingmarks',checkAuthorization,(req,res)=>{
    console.log(req.body);
    dbo=getDb();
    dbo.collection("markscollection").insert(req.body,(err,success)=>{
    if(err)
    {
        console.log('error in admin login')
    }
    else{
        res.json({message:"student marks added successfully"});
    }
});
});


adminroutes.post('/updatefeestatus',checkAuthorization,(req,res)=>{

    console.log(req.body);
    dbo=getDb();
    dbo.collection("feecollection").insertOne(req.body,(err,success)=>{
    if(err)
    {
        console.log('error in admin login')
    }
    else
    {
        res.json({message:"student fee added successfully"});
    }
});

});
adminroutes.post('/updateattendence',checkAuthorization,(req,res)=>{
    console.log(req.body);
    dbo=getDb();
    dbo.collection("attendencecollection").insert(req.body,(err,success)=>{
    if(err)
    {
        console.log('error in admin login');
    }
    else{
        res.json({message:"student attendence added successfully"});
    }
});
});

adminroutes.post('/sendnotification',checkAuthorization,(req,res)=>{
    console.log(req.body);
    dbo=getDb();
    dbo.collection("Notificationcollection").insert(req.body,(err,success)=>{
    if(err)
    {
        console.log('error in admin login');
    }
    else{
        res.json({message:"student notification added successfully"});
    }
});
});
adminroutes.put('/updateprofile',checkAuthorization,(req,res)=>{
    console.log(req.body);
    dbo=getDb();
    dbo.collection("profilecollection").update({username:{$eq:req.body.username}},{$set:{studentno:req.body.studentno,username:req.body.username,course:req.body.course,branch:req.body.branch}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else
        {
            console.log(success);
            res.json({"message":"success"})
        }
    })
});
adminroutes.put('/postingmarks',checkAuthorization,(req,res)=>{
    console.log(req.body);
    dbo=getDb();
    dbo.collection("markscollection").update({username:{$eq:req.body.username}},{$set:{studentno:req.body.studentno,totalmarks:req.body.totalmarks,gainedmarks:req.body.gainedmarks,course:req.body.course,branch:req.body.branch}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else
        {
            console.log(success);
            res.json({"message":"success"})
        }
    })
});
adminroutes.put('/updateattendence',checkAuthorization,(req,res)=>{
    console.log(req.body);
    dbo=getDb();
    dbo.collection("attendencecollection").update({studentname:{$eq:req.body.studentname}},{$set:{studentno:req.body.studentno,monthly:req.body.monthly,weekly:req.body.weekly}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else
        {
            console.log(success);
            res.json({"message":"success"})
        }
    })
});
adminroutes.put('/updatefeestatus',checkAuthorization,(req,res)=>{
    console.log(req.body);
    dbo=getDb();
    dbo.collection("feecollection").update({studentname:{$eq:req.body.studentname}},{$set:{studentno:req.body.studentno,totalfee:req.body.totalfee,paidfee:req.body.paidfee,course:req.body.course,branch:req.body.branch}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else
        {
            console.log(success);
            res.json({"message":"success"})
        }
    })
});
adminroutes.put('/sendnotification',checkAuthorization,(req,res)=>{
    console.log(req.body);
    dbo=getDb();
    dbo.collection("Notificationcollection").update({studentname:{$eq:req.body.studentname}},{$set:{studentno:req.body.studentno,totalfee:req.body.totalfee,paidfee:req.body.paidfee,course:req.body.course,branch:req.body.branch}},(err,success)=>{
        if(err)
        {
            next(err);
        }
        else
        {
            console.log(success);
            res.json({"message":"success"})
        }
    })
});


adminroutes.delete('/deleteprofile/:studentname',checkAuthorization,(req,res)=>{
console.log(req.params);
dbo=getDb();
dbo.collection("profilecollection").deleteOne({studentname:{$eq:req.params.studentname}},(err,dataArray)=>{
    if(err)
    {
        next(err);
    }
    else{
        dbo.collection("profilecollection").find().toArray((err,dataArray)=>{
            if(err)
            {
                next(err);
            }
            else{
                res.json({message:"record deleted",data:dataArray})
            }
        })
    }
})
});


adminroutes.delete('/deletemarks/:studentname',checkAuthorization,(req,res)=>{
    console.log(req.params);
    dbo=getDb();
    dbo.collection("markscollection").deleteOne({studentname:{$eq:req.params.studentname}},(err,dataArray)=>{
        if(err)
        {
            next(err);
        }
        else{
            dbo.collection("markscollection").find().toArray((err,dataArray)=>{
                if(err)
                {
                    next(err);
                }
                else{
                    res.json({message:"record deleted",data:dataArray})
                }
            })
        }
    })
    });
    

adminroutes.delete('/deletefee/:studentname',checkAuthorization,(req,res)=>{
    console.log(req.params);
    dbo=getDb();
    dbo.collection("feecollection").deleteOne({studentname:{$eq:req.params.studentname}},(err,dataArray)=>{
        if(err)
        {
            next(err);
        }
        else{
            dbo.collection("feecollection").find().toArray((err,dataArray)=>{
                if(err)
                {
                    next(err);
                }
                else{
                    res.json({message:"record deleted",data:dataArray})
                }
            })
        }
    })
    });
    adminroutes.delete('/deleteattendence/:studentname',checkAuthorization,(req,res)=>{
        console.log(req.params);
        dbo=getDb();
        dbo.collection("attendencecollection").deleteOne({studentname:{$eq:req.params.studentname}},(err,dataArray)=>{
            if(err)
            {
                next(err);
            }
            else{
                dbo.collection("attendencecollection").find().toArray((err,dataArray)=>{
                    if(err)
                    {
                        next(err);
                    }
                    else{
                        res.json({message:"record deleted",data:dataArray})
                    }
                })
            }
        })
        });
        // adminroutes.get('/sendLeave',checkAuthorization,(req,res)=>{
        //     console.log(req.body);
        //     dbo=getDb();
        //         dbo.collection('leavecollection').find().toArray((err,success)=>{
        //           if(err)
        //             {
        //                 console.log('error in data reading');
        //             }
        //            else if(success.length==0)
        //             {
        //                  res.json({message:"no data found"});   
        //            }
        //             else{
        //                  res.json({message:success});
        //                  console.log(success);
        //            }
        //         })
        // })




        adminroutes.post('/forgotpassword',(req,res,next)=>{
            console.log(req.body)
            dbo=getDb()
            dbo.collection('profilecollection').find({username:req.body.username}).toArray((err,userArray)=>{
                console.log(userArray);
                if(err){
                    next(err)
                }
                else{
                    if(userArray.length===0){
                        res.json({message:"user not found"})
                    }
                    else{
        
                        jwt.sign({username:userArray[0].username},secretkey,{expiresIn:3600},(err,token)=>{
                            if(err){
                             next(err);
                            }
                            else{
                                var OTP=Math.floor(Math.random()*99999)+11111;
                                console.log(OTP)
                                
                                client.messages.create({
                                    body: OTP,
                                    from: '+12054305637', // From a valid Twilio number
                                    to: '+918790871678',  // Text this number
          
                                })
                                .then((message) => {
                                    dbo.collection('OTPCollection').insertOne({
                                        OTP:OTP,
                                        username:userArray[0].username,
                                        OTPGeneratedTime:new Date().getTime()+15000
                                },(err,success)=>{
                                    if(err){
                                        next(err)
                                    }
                                    else{                                        
                                        res.json({"message":"user found",
                                            "token":token,
                                            "OTP":OTP,
                                            "username":userArray[0].username
                                        })
                                    }
                                })
                                });
        
                            }
                            
                        })
                    }
                }
            })
        })
//verify OTP
adminroutes.post('/verifyotp',(req,res,next)=>{
    console.log(req.body)
    console.log(new Date().getTime())
    var currentTime=new Date().getTime()
    dbo.collection('OTPCollection').find({"OTP":req.body.OTP}).toArray((err,OTPArray)=>{
        if(err){
            next(err)
        }
        else if(OTPArray.length===0){
            res.json({"message":"invalidOTP"})
        }
        else if(OTPArray[0].OTPGeneratedTime < req.body.currentTime){
            res.json({"message":"invalidOTP"})
        }
        else{
            
            dbo.collection('OTPCollection').deleteOne({OTP:req.body.OTP},(err,success)=>{
                if(err){
                    next(err);
                }
                else{
                    console.log(OTPArray)
                    res.json({"message":"verifiedOTP"})
                }
            })
        }
    })
  })
  adminroutes.put('/changepassword',(req,res,next)=>{
    console.log(req.body)
    dbo=getDb()
    bcrypt.hash(req.body.password,6,(err,hashedPassword)=>{
        if (err) {
            next(err)
        } else {
            console.log(hashedPassword)
            dbo.collection('profilecollection').updateOne({username:req.body.username},{$set:{
                password:hashedPassword
            }},(err,success)=>{
                if(err){
                    next(err)
                }
                else{
                    res.json({"message":"password changed"})
                }
            }) 
        }
    })
    
  })
//   adminroutes.post('/saverequest',(req,res,next)=>
// {
//   dbo=getdb()
//   console.log(req.body)
//   if(req.body=={})
//   {
//       res.json({message:"server did not receive data"})
//   }else
//   {
//     dbo.collection("requestcollection").insertOne(req.body,(err,success)=>{
//         if(err){
//           next(err)
//         }


//         else{
           
//             dbo.collection("leavecollection").deleteOne({username:{$eq:req.body.username}},(err,dataArray)=>{
//               if(err)
//               {
//                 next(err)
//               }
//               else
//               {
//                  res.json({message:"Request Sent Successfully"})
//                  res.json({data:dataArray})
//               }
//             })
//         }
//     })
//   }
    
// })
 adminroutes.post('/savereq',checkAuthorization,(req,res,next)=>
{
  dbo=getDb()
  if(req.body=={})
  {
      res.json({message:"server did not receive data"})
  }else
  {
    dbo.collection("rescollection").insertOne(req.body,(err,dataArray)=>{
        if(err){
          next(err)
        }
        else{
           
            dbo.collection("reqcollection").deleteOne({username:{$eq:req.body.username}},(err,dataArray)=>{
              if(err)
              {
                next(err)
              }
              else
              {
                 res.json({message:"successfully inserted"})
              }
            })
        }
    })
  }
    
})
 adminroutes.get('/readreq',checkAuthorization,(req,res,next)=>
  {
    dbo=getDb()
    dbo.collection("reqcollection").find().toArray((err,dataArray)=>{
    if(err)
    {
      next(err)
    }
    else
    {
      res.json({message:dataArray})
    }
  
  })

  })  
//exporting adminroutes module
module.exports=adminroutes;